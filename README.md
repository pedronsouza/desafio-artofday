# Art of the day Android App


App para resolução do teste da zazcar. Usando APIs públicas, tenha acesso
a obras de artes de museus do mundo no seu celular. Baixe e inspiri-se


## Ideia
Um aplicativo que utilize a api pública do [Rijksmuseum] (https://www.rijksmuseum.nl/en/api)
para exibir obras de arte do mundo todo para quem quer se inspirar.

## Arquitetura

A estrutura foi definida inspirada através do (Clean Architecture) [https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html]
Também utilizei programação reativa (RxJava) para definir o fluxo de dados do aplicativo e eventos de views.

### Breve explicação sobre a arquitetura

#### Dependency Injection
Optei utilizar o Dagger para definir o grafo de objetos e ajudar a injetar as dependencias 'on the fly'


#### DataSource
Objeto utilizado para criar uma abstração entre o objeto que requer o dado (Repositories) e a fonte
de dado em sí. O Dagger é responsavel por injetar a instância concreta do data source, tornando
o gerenciamento de qual datasource servir em qual momento, centralizado no módulo de dados


#### Repositories
Objetos que realizam o acesso a dados, e dependem diretamente do DataSource.
Comumente servem aos Interactors

#### Interactors
Objetos responsáveis por executar tarefas, comumente requisitados por um objeto presenter

#### Screen
Objetos que definem os comportamentos de input do usuário e de como responder a resposta
de ações de outras dependencias. São os ViewControllers do framework (ex: Activities, Fragments).

#### Presenters
Objetos responsáveis por criar uma ponte de comunicação entre a Screen e o Dominio da aplicação.
Ajudam a aplicar regras de negócio e preparar os dados para a view



## Observações

- Não consegui ter tempo hábil para escrever o suite de testes. Coloquei as refs
do stack de testes no gradle mas não consegui colocar cobertura de testes. Posso
indicar outros repos de codigos meus pra que vcs possam ver como eu escrevo testes
geralmente.