package br.pedronsouza.artofday;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import br.pedronsouza.artofday.domain.di.ApplicationComponent;
import br.pedronsouza.artofday.domain.di.DaggerApplicationComponent;
import br.pedronsouza.artofday.domain.di.modules.ApplicationModule;
import br.pedronsouza.artofday.domain.di.modules.DataModule;
import br.pedronsouza.artofday.domain.di.modules.NetModule;

public class ArtOfDayApplication extends Application {
    public static ApplicationComponent applicationComponent;
    public final static int defaulPageSize = 30;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);

        applicationComponent = DaggerApplicationComponent.builder()
                .netModule(new NetModule(this))
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule())
                .build();
    }
}
