package br.pedronsouza.artofday.domain.di.modules;


import android.content.Context;

import javax.inject.Singleton;

import br.pedronsouza.artofday.data.repositories.PaintingRepository;
import br.pedronsouza.artofday.domain.interactors.FetchPaintingsListInteractor;
import br.pedronsouza.artofday.domain.interactors.executors.PostExecutionThread;
import br.pedronsouza.artofday.domain.interactors.executors.ThreadExecutor;
import br.pedronsouza.artofday.domain.interactors.executors.impl.BackgroundThreadExecutor;
import br.pedronsouza.artofday.domain.interactors.executors.impl.MainThreadExecutor;
import br.pedronsouza.artofday.ui.presenters.PaintingListPresenter;
import dagger.Module;
import dagger.Provides;

@Singleton
@Module(includes = {NetModule.class, DataModule.class})
public class ApplicationModule {
    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides @Singleton public ThreadExecutor providesThreadExecutor() {
        return new BackgroundThreadExecutor();
    }

    @Provides @Singleton public PostExecutionThread providesPostExecutionThread() {
        return new MainThreadExecutor();
    }

    @Provides public FetchPaintingsListInteractor providesFetchPaintingsListInteractor(PaintingRepository repository,
                                                                                       ThreadExecutor executor,
                                                                                       PostExecutionThread postExecutionThread) {
        return new FetchPaintingsListInteractor(repository, executor, postExecutionThread);
    }


    @Provides public PaintingListPresenter providesPaintingListPresenter(FetchPaintingsListInteractor interactor) {
        return new PaintingListPresenter(interactor);
    }
}
