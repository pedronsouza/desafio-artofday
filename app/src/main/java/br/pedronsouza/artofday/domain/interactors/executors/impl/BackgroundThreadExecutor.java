package br.pedronsouza.artofday.domain.interactors.executors.impl;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import br.pedronsouza.artofday.domain.interactors.executors.ThreadExecutor;

public class BackgroundThreadExecutor implements ThreadExecutor {
    @Inject public BackgroundThreadExecutor() {}
    @Override public void execute(@NonNull Runnable runnable) { AsyncTask.execute(runnable);}
}
