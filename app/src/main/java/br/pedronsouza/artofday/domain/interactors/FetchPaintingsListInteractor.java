package br.pedronsouza.artofday.domain.interactors;

import javax.inject.Inject;

import br.pedronsouza.artofday.data.repositories.PaintingRepository;
import br.pedronsouza.artofday.domain.interactors.executors.PostExecutionThread;
import br.pedronsouza.artofday.domain.interactors.executors.ThreadExecutor;
import br.pedronsouza.artofday.domain.viewmodels.LatestPaintingsViewModel;
import rx.Observable;

public class FetchPaintingsListInteractor extends BaseInteractor<LatestPaintingsViewModel> {
    private PaintingRepository paintingRepository;
    private int page = 1;

    public void setPage(int page) {
        this.page = page;
    }

    @Inject public FetchPaintingsListInteractor(PaintingRepository paintingRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.paintingRepository = paintingRepository;
    }

    @Override
    protected Observable<LatestPaintingsViewModel> observableForInteractor() {
        return paintingRepository.getLatestPaitings(page);
    }
}
