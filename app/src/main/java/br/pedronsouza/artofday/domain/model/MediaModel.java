package br.pedronsouza.artofday.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

public class MediaModel implements Parcelable {
    @Expose private String guid;
    @Expose private int offsetPercentageX;
    @Expose private int offsetPercentageY;
    @Expose private int width;
    @Expose private int height;
    @Expose private String url;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getOffsetPercentageX() {
        return offsetPercentageX;
    }

    public void setOffsetPercentageX(int offsetPercentageX) {
        this.offsetPercentageX = offsetPercentageX;
    }

    public int getOffsetPercentageY() {
        return offsetPercentageY;
    }

    public void setOffsetPercentageY(int offsetPercentageY) {
        this.offsetPercentageY = offsetPercentageY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static Creator<MediaModel> getCREATOR() {
        return CREATOR;
    }

    public MediaModel(Parcel in) {
        guid = in.readString();
        offsetPercentageX = in.readInt();
        offsetPercentageY = in.readInt();
        width = in.readInt();
        height = in.readInt();
        url = in.readString();
    }

    public static final Creator<MediaModel> CREATOR = new Creator<MediaModel>() {
        @Override
        public MediaModel createFromParcel(Parcel in) {
            return new MediaModel(in);
        }

        @Override
        public MediaModel[] newArray(int size) {
            return new MediaModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(guid);
        parcel.writeInt(offsetPercentageX);
        parcel.writeInt(offsetPercentageY);
        parcel.writeInt(width);
        parcel.writeInt(height);
        parcel.writeString(url);
    }
}
