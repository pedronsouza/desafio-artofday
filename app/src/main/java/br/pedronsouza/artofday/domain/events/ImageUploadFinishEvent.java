package br.pedronsouza.artofday.domain.events;

import br.pedronsouza.artofday.domain.model.PaintingModel;

/**
 * Created by pedronsouza on 02/12/16.
 */

public class ImageUploadFinishEvent {
    private boolean isSuccess;
    private PaintingModel model;

    public ImageUploadFinishEvent(boolean isSuccess, PaintingModel model) {
        this.isSuccess = isSuccess;
        this.model = model;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public PaintingModel getModel() {
        return model;
    }

    public void setModel(PaintingModel model) {
        this.model = model;
    }
}
