package br.pedronsouza.artofday.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaintingModel implements Parcelable {
    @Expose String id;
    @Expose String objectNumber;
    @Expose String title;
    @Expose String principalOrFirstMaker;
    @Expose boolean showImage;
    @Expose boolean permitDownload;
    @Expose @SerializedName("headerImage") MediaModel imageInfo;
    @Expose @SerializedName("webImage") MediaModel detailImageInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObjectNumber() {
        return objectNumber;
    }

    public void setObjectNumber(String objectNumber) {
        this.objectNumber = objectNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isShowImage() {
        return showImage;
    }

    public void setShowImage(boolean showImage) {
        this.showImage = showImage;
    }

    public String getPrincipalOrFirstMaker() {
        return principalOrFirstMaker;
    }

    public void setPrincipalOrFirstMaker(String principalOrFirstMaker) {
        this.principalOrFirstMaker = principalOrFirstMaker;
    }

    public boolean isPermitDownload() {
        return permitDownload;
    }

    public void setPermitDownload(boolean permitDownload) {
        this.permitDownload = permitDownload;
    }

    public MediaModel getImageInfo() {
        return imageInfo;
    }

    public void setImageInfo(MediaModel imageInfo) {
        this.imageInfo = imageInfo;
    }

    public MediaModel getDetailImageInfo() {
        return detailImageInfo;
    }

    public void setDetailImageInfo(MediaModel detailImageInfo) {
        this.detailImageInfo = detailImageInfo;
    }

    public static Creator<PaintingModel> getCREATOR() {
        return CREATOR;
    }

    public PaintingModel(Parcel in) {
        id = in.readString();
        objectNumber = in.readString();
        title = in.readString();
        principalOrFirstMaker = in.readString();
        showImage = in.readByte() != 0;
        permitDownload = in.readByte() != 0;
        imageInfo = in.readParcelable(MediaModel.class.getClassLoader());
        detailImageInfo = in.readParcelable(MediaModel.class.getClassLoader());
    }

    public static final Creator<PaintingModel> CREATOR = new Creator<PaintingModel>() {
        @Override
        public PaintingModel createFromParcel(Parcel in) {
            return new PaintingModel(in);
        }

        @Override
        public PaintingModel[] newArray(int size) {
            return new PaintingModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(objectNumber);
        parcel.writeString(title);
        parcel.writeString(principalOrFirstMaker);
        parcel.writeByte((byte) (showImage ? 1 : 0));
        parcel.writeByte((byte) (permitDownload ? 1 : 0));
        parcel.writeParcelable(imageInfo, i);
        parcel.writeParcelable(detailImageInfo, i);
    }
}
