package br.pedronsouza.artofday.domain.interactors.executors;

import java.util.concurrent.Executor;

public interface ThreadExecutor extends Executor {
}
