package br.pedronsouza.artofday.domain.di;

import javax.inject.Singleton;

import br.pedronsouza.artofday.domain.di.modules.ApplicationModule;
import br.pedronsouza.artofday.domain.di.modules.DataModule;
import br.pedronsouza.artofday.domain.di.modules.NetModule;
import br.pedronsouza.artofday.ui.activities.BaseActivity;
import br.pedronsouza.artofday.ui.activities.MainActivity;
import dagger.Component;

@Singleton
@Component(modules = {NetModule.class, DataModule.class, ApplicationModule.class})
public interface ApplicationComponent {
    void inject(BaseActivity activity);
    void inject(MainActivity activity);
}
