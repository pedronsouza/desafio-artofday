package br.pedronsouza.artofday.domain.interactors.executors.impl;

import javax.inject.Inject;

import br.pedronsouza.artofday.domain.interactors.executors.PostExecutionThread;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

public class MainThreadExecutor implements PostExecutionThread {
    @Inject public MainThreadExecutor() {}
    @Override public Scheduler scheduler() { return AndroidSchedulers.mainThread(); }
}
