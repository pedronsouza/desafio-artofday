package br.pedronsouza.artofday.domain.screens;


import android.support.annotation.NonNull;

import br.pedronsouza.artofday.domain.viewmodels.LatestPaintingsViewModel;
import br.pedronsouza.artofday.ui.activities.BaseActivity;

public interface PaintingsListScreen {
    void getLatestPaintings();
    void onLatestPaintingsFetchedSuccess(@NonNull LatestPaintingsViewModel viewModel);
    void onLatestPaintingsFetchedError(@NonNull Throwable err);
}
