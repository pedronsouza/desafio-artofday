package br.pedronsouza.artofday.domain.interactors.executors;

import rx.Scheduler;

public interface PostExecutionThread {
    Scheduler scheduler();
}
