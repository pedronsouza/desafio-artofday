package br.pedronsouza.artofday.domain.screens;

/**
 * Created by pedronsouza on 02/12/16.
 */

public interface DetailsScreen {
    void onSaveToGalleryClicked();
    void onShareContentClicked();
    void onSavePermissionDenied();

}
