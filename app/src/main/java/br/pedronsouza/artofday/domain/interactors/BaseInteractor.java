package br.pedronsouza.artofday.domain.interactors;

import br.pedronsouza.artofday.domain.interactors.executors.PostExecutionThread;
import br.pedronsouza.artofday.domain.interactors.executors.ThreadExecutor;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;

public abstract class BaseInteractor<T> {
    private ThreadExecutor threadExecutor;
    private PostExecutionThread postExecutionThread;
    private Subscription subscription;

    protected abstract Observable<T> observableForInteractor();

    public BaseInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
    }

    public void execute(Subscriber<T> subscriber) {
        this.subscription = observableForInteractor()
                .observeOn(postExecutionThread.scheduler())
                .subscribeOn(Schedulers.from(threadExecutor))
                .subscribe(subscriber);
    }

    public void unsubscribe() {
        if (this.subscription != null) {
            this.subscription.unsubscribe();
        }
    }
}
