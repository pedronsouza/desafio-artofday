package br.pedronsouza.artofday.domain.viewmodels;

import java.util.List;

import br.pedronsouza.artofday.domain.model.PaintingModel;

public class LatestPaintingsViewModel {
    private boolean hasMorePages;
    private int actualPage;
    private List<PaintingModel> items;

    public LatestPaintingsViewModel() {}

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

    public int getActualPage() {
        return actualPage;
    }

    public void setActualPage(int actualPage) {
        this.actualPage = actualPage;
    }

    public List<PaintingModel> getItems() {
        return items;
    }

    public void setItems(List<PaintingModel> items) {
        this.items = items;
    }

    public LatestPaintingsViewModel(boolean hasMorePages, int actualPage, List<PaintingModel> items) {
        this.hasMorePages = hasMorePages;
        this.actualPage = actualPage;
        this.items = items;
    }
}
