package br.pedronsouza.artofday.domain.di.modules;

import javax.inject.Singleton;

import br.pedronsouza.artofday.data.datasources.PaintingDataSource;
import br.pedronsouza.artofday.data.datasources.impl.RemotePaintingDataSource;
import br.pedronsouza.artofday.data.net.ApiGateway;
import br.pedronsouza.artofday.data.repositories.PaintingRepository;
import dagger.Module;
import dagger.Provides;

@Module(includes = {NetModule.class})
public class DataModule {
    @Provides @Singleton public PaintingDataSource providesPaintingDataSource(ApiGateway gateway) {
        return new RemotePaintingDataSource(gateway);
    }

    @Provides public PaintingRepository providesPaintingsRepository(PaintingDataSource dataSource) {
        return new PaintingRepository(dataSource);
    }
}
