package br.pedronsouza.artofday.ui.fragments;


import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import br.pedronsouza.artofday.R;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import br.pedronsouza.artofday.ui.support.Finder;

public class ArtWorkFragment extends BaseFragment {
    public static final String MODEL_EXTRA =  "MODEL_EXTRA";

    private SimpleDraweeView image;
    private TextView title;
    private ProgressBar progress;
    private PaintingModel model;

    public static ArtWorkFragment newInstance(PaintingModel model) {
        ArtWorkFragment fragment = new ArtWorkFragment();
        Bundle args = new Bundle();
        args.putParcelable(MODEL_EXTRA, model);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    int layoutResourceId() {
        return R.layout.view_artwork_portrait;
    }

    @Override
    void onViewsBind() {
        image = Finder.findViewById(viewGroup, R.id.image);
        title = Finder.findViewById(viewGroup, R.id.title);
        progress = Finder.findViewById(viewGroup, R.id.progress);

        if (getArguments() != null) {
            model = getArguments().getParcelable(MODEL_EXTRA);

            if (model != null) {
                double width = model.getImageInfo().getWidth() * 0.6;
                double height = model.getImageInfo().getWidth() * 0.6;
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(model.getImageInfo().getUrl()))
                        .setLocalThumbnailPreviewsEnabled(true)
                        .setProgressiveRenderingEnabled(true)
                        .setResizeOptions(new ResizeOptions((int)width, (int)height))
                        .build();


                image.setController(Fresco.newDraweeControllerBuilder().setImageRequest(request).setControllerListener(new BaseControllerListener<ImageInfo>(){
                    @Override
                    public void onSubmit(String id, Object callerContext) {
                        progress.setVisibility(View.VISIBLE);
                        super.onSubmit(id, callerContext);
                    }

                    @Override
                    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                        title.setText(model.getTitle());
                        progress.setVisibility(View.GONE);
                        super.onFinalImageSet(id, imageInfo, animatable);
                    }
                }).setOldController(image.getController()).build());
            }
        }

    }

    @Override
    void onSubscribe() {

    }

    @Override
    void onUnsubscribe() {

    }

    public interface Listener {
        void onJumpToNextClicked();
    }
}
