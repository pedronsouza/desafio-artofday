package br.pedronsouza.artofday.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.DraweeTransition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

import br.pedronsouza.artofday.ArtOfDayApplication;
import br.pedronsouza.artofday.R;
import br.pedronsouza.artofday.domain.events.ImageUploadFinishEvent;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import br.pedronsouza.artofday.ui.services.ImageDownloadService;
import br.pedronsouza.artofday.ui.support.Finder;

public abstract class BaseActivity extends AppCompatActivity {
    abstract int layoutResourceId();
    abstract void onViewsBind(@Nullable Bundle savedInstanceState);
    abstract void onSubscribe();
    abstract void onUnsubscribe();

    protected void inject() {
        ArtOfDayApplication.applicationComponent.inject(this);
    }
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        setContentView(layoutResourceId());
        toolbar = Finder.findViewById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementEnterTransition(DraweeTransition.createTransitionSet(ScalingUtils.ScaleType.CENTER_CROP, ScalingUtils.ScaleType.FIT_CENTER));
            getWindow().setSharedElementExitTransition(DraweeTransition.createTransitionSet(ScalingUtils.ScaleType.FIT_CENTER, ScalingUtils.ScaleType.CENTER_CROP));
        }

        onViewsBind(savedInstanceState);
    }

    protected void onUnknownError(Runnable retry) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert)
                .setMessage(R.string.unknow_error)
                .setNegativeButton(R.string.cancel, (view, which) -> view.dismiss())
                .setPositiveButton(R.string.try_again, (view, which) -> retry.run()).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onImageDownloadFinished(ImageUploadFinishEvent event) {
        String msg;
        if (event.isSuccess()) {
            msg = String.format(Locale.getDefault(), "%s foi salvo com sucesso :)", event.getModel().getTitle());
            Snackbar.make(toolbar.getRootView(), msg, Snackbar.LENGTH_LONG).show();
        } else {
            msg = String.format(Locale.getDefault(), "Ocorreu um problema ao salvar a imagem %s", event.getModel().getTitle());
            Snackbar.make(toolbar.getRootView(), msg, Snackbar.LENGTH_LONG).setAction(R.string.try_again, (view) -> {
                startDownloadService(event.getModel());
            }).show();
        }
    }

    protected void startDownloadService(PaintingModel model) {
        Intent intent = new Intent(this, ImageDownloadService.class);
        intent.putExtra(ImageDownloadService.ART_TO_DOWNLOAD_EXTRA, model);
        startService(intent);
        Snackbar.make(toolbar.getRootView(), "Salvando imagem na galeria", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onSubscribe();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        onUnsubscribe();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public enum ViewState {
        IDLE,
        LOADING,
        ERROR,
        SUCCESS
    }
}
