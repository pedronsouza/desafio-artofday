package br.pedronsouza.artofday.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

abstract public class BaseFragment extends Fragment {
    protected  @Nullable View viewGroup;

    abstract int layoutResourceId();
    abstract void onViewsBind();
    abstract void onSubscribe();
    abstract void onUnsubscribe();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewGroup = inflater.inflate(layoutResourceId(), container, false);
        onViewsBind();
        return viewGroup;
    }

    @Override
    public void onResume() {
        super.onResume();
        onSubscribe();
    }

    @Override
    public void onStop() {
        onUnsubscribe();
        super.onStop();
    }

}
