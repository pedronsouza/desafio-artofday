package br.pedronsouza.artofday.ui.support;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Finder {
    public static <T extends View> T findViewById(AppCompatActivity context, int id) {
        return (T)context.findViewById(id);
    }

    public static <T> T findViewById(View view, int id) {
        return (T)view.findViewById(id);
    }
}
