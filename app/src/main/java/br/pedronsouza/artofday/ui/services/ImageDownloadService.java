package br.pedronsouza.artofday.ui.services;


import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import br.pedronsouza.artofday.domain.events.ImageUploadFinishEvent;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ImageDownloadService extends IntentService {
    public static final String ART_TO_DOWNLOAD_EXTRA = "IMAGE_DOWNLOAD_EXTRA";
    private static final String SERVICE_NAME = "ImageDownloadService";

    public ImageDownloadService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        PaintingModel model = intent.getParcelableExtra(ART_TO_DOWNLOAD_EXTRA);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(model.getDetailImageInfo().getUrl())
                .build();
        Response response = null;

        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (response != null && response.isSuccessful()) {
            Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
            String title = model.getTitle().replace(" ", "_");
            MediaStore.Images.Media.insertImage(getContentResolver(), bmp, title, "");
            EventBus.getDefault().post(new ImageUploadFinishEvent(true, model));
        } else {
            EventBus.getDefault().post(new ImageUploadFinishEvent(false, model));
        }
    }
}
