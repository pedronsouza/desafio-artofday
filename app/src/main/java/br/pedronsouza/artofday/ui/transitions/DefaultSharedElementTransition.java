package br.pedronsouza.artofday.ui.transitions;

import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.TransitionSet;

public class DefaultSharedElementTransition extends TransitionSet {
    public DefaultSharedElementTransition() {
        setOrdering(TransitionSet.ORDERING_TOGETHER);
        addTransition(new ChangeBounds());
        addTransition(new ChangeImageTransform());
    }
}
