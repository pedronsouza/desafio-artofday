package br.pedronsouza.artofday.ui.presenters;

import javax.inject.Inject;

import br.pedronsouza.artofday.domain.interactors.FetchPaintingsListInteractor;
import br.pedronsouza.artofday.domain.screens.PaintingsListScreen;
import br.pedronsouza.artofday.domain.viewmodels.LatestPaintingsViewModel;
import rx.Subscriber;

public class PaintingListPresenter implements Presenter {
    private FetchPaintingsListInteractor fetchPaintingsListInteractor;

    @Inject public PaintingListPresenter(FetchPaintingsListInteractor interactor) {
        this.fetchPaintingsListInteractor = interactor;
    }

    @Override
    public void unsubscribe() {
        this.fetchPaintingsListInteractor.unsubscribe();
    }

    public void getLatestPaitings(int page, PaintingsListScreen screen) {
        this.fetchPaintingsListInteractor.setPage(page);
        this.fetchPaintingsListInteractor.execute(new LatestPaintingsSubscriber(screen));
    }

    private class LatestPaintingsSubscriber extends Subscriber<LatestPaintingsViewModel> {
        private PaintingsListScreen screen;

        LatestPaintingsSubscriber(PaintingsListScreen screen) {
            this.screen = screen;
        }

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            this.screen.onLatestPaintingsFetchedError(e);
        }

        @Override
        public void onNext(LatestPaintingsViewModel latestPaintingsViewModel) {
            this.screen.onLatestPaintingsFetchedSuccess(latestPaintingsViewModel);
        }
    }
}
