package br.pedronsouza.artofday.ui.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.jakewharton.rxbinding.support.v4.widget.RxSwipeRefreshLayout;

import javax.inject.Inject;

import br.pedronsouza.artofday.ArtOfDayApplication;
import br.pedronsouza.artofday.R;
import br.pedronsouza.artofday.domain.screens.PaintingsListScreen;
import br.pedronsouza.artofday.domain.viewmodels.LatestPaintingsViewModel;
import br.pedronsouza.artofday.ui.adapters.PaintingsAdapter;
import br.pedronsouza.artofday.ui.presenters.PaintingListPresenter;
import br.pedronsouza.artofday.ui.support.Finder;
import br.pedronsouza.artofday.ui.widgets.EndlessRecyclerView;
import rx.Subscription;

public class MainActivity extends BaseActivity implements PaintingsListScreen, EndlessRecyclerView.OnLoadMore {
    @Inject PaintingListPresenter presenter;
    private int page = 1;

    private PaintingsAdapter adapter;
    private boolean shouldLoadMore = true;
    private SwipeRefreshLayout swipe;
    private EndlessRecyclerView recyclerView;

    private Subscription subsSwipe;

    @Override
    protected void inject() {
        super.inject();
        ArtOfDayApplication.applicationComponent.inject(this);
    }

    @Override
    int layoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    void onViewsBind(@Nullable Bundle savedInstanceState) {
        swipe = Finder.findViewById(this, R.id.swipe);
        recyclerView = Finder.findViewById(this, R.id.recycler);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.latest_paintings);
        }

        toolbar.setTitle(R.string.latest_paintings);

        getLatestPaintings();
    }

    @Override
    protected void onSubscribe() {
        subsSwipe = RxSwipeRefreshLayout.refreshes(swipe).subscribe(nothing -> {
            page = 1;
            adapter = null;
            shouldLoadMore = true;
            getLatestPaintings();
        });
    }

    @Override
    protected void onUnsubscribe() {
        if (subsSwipe != null) subsSwipe.unsubscribe();
        presenter.unsubscribe();
    }

    @Override
    public void getLatestPaintings() {
        presenter.getLatestPaitings(page, this);
    }

    @Override
    public void onLatestPaintingsFetchedSuccess(@NonNull LatestPaintingsViewModel viewModel) {
        shouldLoadMore = viewModel.isHasMorePages();

        if (adapter == null) {
            adapter = new PaintingsAdapter(viewModel.getItems());
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            recyclerView.setAdapter(adapter);
        } else {
            adapter.getItems().addAll(viewModel.getItems());
            int last = adapter.getItems().size() - 1;
            adapter.notifyItemRangeInserted(last, viewModel.getItems().size());
        }
    }

    @Override
    public void onLatestPaintingsFetchedError(@NonNull Throwable err) {
        onUnknownError(this::getLatestPaintings);
    }

    @Override
    public void loadMore(int currentPage) {
        if (shouldLoadMore) {
            page++;
            presenter.getLatestPaitings(page, this);
        }
    }
}
