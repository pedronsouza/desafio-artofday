package br.pedronsouza.artofday.ui.adapters;

import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

import br.pedronsouza.artofday.R;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import br.pedronsouza.artofday.ui.activities.BaseActivity;
import br.pedronsouza.artofday.ui.activities.DetailsActivity;
import br.pedronsouza.artofday.ui.support.Finder;


public class PaintingsAdapter extends RecyclerView.Adapter<PaintingsAdapter.PaintingViewHolder> {
    private final List<PaintingModel> items;

    public List<PaintingModel> getItems() {
        return items;
    }

    public PaintingsAdapter(List<PaintingModel> items) {
        this.items = items;
    }

    @Override
    public PaintingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int resId = R.layout.view_artwork_portrait;
        View view = LayoutInflater.from(parent.getContext()).inflate(resId, parent, false);
        return new PaintingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaintingViewHolder holder, int position) {
        holder.update(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class PaintingViewHolder extends RecyclerView.ViewHolder {
        private final SimpleDraweeView image;
        private final TextView title;
        private final ProgressBar progress;

        PaintingViewHolder(View itemView) {
            super(itemView);
            image = Finder.findViewById(itemView, R.id.image);
            title = Finder.findViewById(itemView, R.id.title);
            progress = Finder.findViewById(itemView, R.id.progress);
        }

        void update(final PaintingModel model) {
            double width = model.getImageInfo().getWidth() * 0.6;
            double height = model.getImageInfo().getWidth() * 0.6;
            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(model.getImageInfo().getUrl()))
                    .setLocalThumbnailPreviewsEnabled(true)
                    .setProgressiveRenderingEnabled(true)
                    .setResizeOptions(new ResizeOptions((int)width, (int)height))
                    .build();

            View.OnClickListener clickListener = (view) -> {
                Intent intent = new Intent(itemView.getContext(), DetailsActivity.class);
                intent.putExtra(DetailsActivity.DETAIL_EXTRA, model);
                Pair<View, String> p1 = Pair.create(title, itemView.getContext().getString(R.string.painting_name_transtion_name));
                Pair<View, String> p2 = Pair.create(image, itemView.getContext().getString(R.string.painting_transtion_name));
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((BaseActivity)itemView.getContext(), p1, p2);

                itemView.getContext().startActivity(intent, options.toBundle());
            };

            itemView.setOnClickListener(clickListener);
            image.setOnClickListener(clickListener);
            title.setOnClickListener(clickListener);

            image.setController(Fresco.newDraweeControllerBuilder().setImageRequest(request).setControllerListener(new BaseControllerListener<ImageInfo>(){
                @Override
                public void onSubmit(String id, Object callerContext) {
                    progress.setVisibility(View.VISIBLE);
                    super.onSubmit(id, callerContext);
                }

                @Override
                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                    title.setText(model.getTitle());
                    progress.setVisibility(View.GONE);
                    super.onFinalImageSet(id, imageInfo, animatable);
                }
            }).setOldController(image.getController()).build());
        }
    }
}
