package br.pedronsouza.artofday.ui.presenters;

/**
 * Created by pedronsouza on 02/12/16.
 */

public interface Presenter {
    void unsubscribe();
}
