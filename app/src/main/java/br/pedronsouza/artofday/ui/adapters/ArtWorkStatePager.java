package br.pedronsouza.artofday.ui.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import br.pedronsouza.artofday.domain.model.PaintingModel;
import br.pedronsouza.artofday.ui.fragments.ArtWorkFragment;

public class ArtWorkStatePager extends FragmentStatePagerAdapter {
    private List<PaintingModel> items;

    public void addItems(List<PaintingModel> items) {
        this.items.addAll(items);
    }

    public ArtWorkStatePager(List<PaintingModel> items, FragmentManager fm) {
        super(fm);
        this.items = items;
    }

    @Override
    public Fragment getItem(int position) {
        return ArtWorkFragment.newInstance(this.items.get(position));
    }

    @Override
    public int getCount() {
        return this.items.size();
    }
}
