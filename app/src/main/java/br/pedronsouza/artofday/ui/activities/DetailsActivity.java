package br.pedronsouza.artofday.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Locale;

import br.pedronsouza.artofday.R;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import br.pedronsouza.artofday.domain.screens.DetailsScreen;
import br.pedronsouza.artofday.ui.support.Finder;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import rx.Subscription;

@RuntimePermissions
public class DetailsActivity extends BaseActivity implements DetailsScreen {
    public static final String DETAIL_EXTRA = "detail_extra";
    private PaintingModel model;
    private Button shareBt;
    private Button saveImg;
    private SimpleDraweeView image;
    private ProgressBar progress;
    private TextView title;
    private TextView author;

    private Subscription shareBtSubs;
    private Subscription saveImgBtSubs;

    @Override
    int layoutResourceId() {
        return R.layout.activity_details;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        DetailsActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    void onViewsBind(@Nullable Bundle savedInstanceState) {
        shareBt = Finder.findViewById(this, R.id.share_bt);
        saveImg = Finder.findViewById(this, R.id.save_img);
        image = Finder.findViewById(this, R.id.image);
        progress = Finder.findViewById(this, R.id.progress);
        title = Finder.findViewById(this, R.id.title);
        author = Finder.findViewById(this, R.id.author);

        if (getIntent().hasExtra(DETAIL_EXTRA)) {
            model = getIntent().getParcelableExtra(DETAIL_EXTRA);

            title.setText(model.getTitle());
            author.setText(model.getPrincipalOrFirstMaker());

            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(model.getTitle());
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }

            if (model.isPermitDownload()) {
                saveImg.setVisibility(View.VISIBLE);
            } else {
                saveImg.setVisibility(View.GONE);
            }

            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(model.getDetailImageInfo().getUrl()))
                    .disableDiskCache()
                    .setProgressiveRenderingEnabled(true)
                    .build();


            image.setController(Fresco.newDraweeControllerBuilder().setImageRequest(request).setControllerListener(new BaseControllerListener<ImageInfo>(){
                @Override
                public void onSubmit(String id, Object callerContext) {
                    progress.setVisibility(View.VISIBLE);
                    super.onSubmit(id, callerContext);
                }

                @Override
                public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                    progress.setVisibility(View.GONE);
                    super.onFinalImageSet(id, imageInfo, animatable);
                }
            }).setOldController(image.getController()).build());
        } else {
            onUnknownError(this::onBackPressed);
        }



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    void onSubscribe() {
        if (model != null) {
            shareBtSubs = RxView.clicks(shareBt).subscribe((view) -> {
                onShareContentClicked();
            });

            saveImgBtSubs = RxView.clicks(saveImg).subscribe((view) -> {
                DetailsActivityPermissionsDispatcher.onSaveToGalleryClickedWithCheck(DetailsActivity.this);
            });
        }
    }

    @Override
    void onUnsubscribe() {
        if (shareBtSubs != null) shareBtSubs.unsubscribe();
        if (saveImgBtSubs != null) saveImgBtSubs.unsubscribe();
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    @Override
    public void onSaveToGalleryClicked() {
        startDownloadService(model);
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    @Override
    public void onSavePermissionDenied() {
        Snackbar.make(toolbar.getRootView(), "Preciso de acesso ao seu armazenamento interno para salvar as imagens =(", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onShareContentClicked() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, String.format(Locale.getDefault(), "Veja essa referência do art of day: %s", model.getImageInfo().getUrl()));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }
}
