package br.pedronsouza.artofday.data.net;

import javax.inject.Inject;

import br.pedronsouza.artofday.data.net.endpoints.PaintingsEndpoint;
import retrofit2.Retrofit;

public class ApiGateway {
    private Retrofit retrofit;

    @Inject public ApiGateway(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public PaintingsEndpoint paitings() {
        return this.retrofit.create(PaintingsEndpoint.class);
    }
}
