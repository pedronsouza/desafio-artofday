package br.pedronsouza.artofday.data.net.helpers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.List;

public class PaginatedResponse<T extends Parcelable> implements Parcelable {
    @Expose private List<T> artObjects;
    @Expose private long count;

    public List<T> getArtObjects() {
        return artObjects;
    }

    public void setArtObjects(List<T> artObjects) {
        this.artObjects = artObjects;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public PaginatedResponse(Parcel in) {
        final Class<?> type = (Class<?>) in.readSerializable();
        in.readList(artObjects, type.getClassLoader());
        count = in.readLong();
    }

    public static final Creator<PaginatedResponse> CREATOR = new Creator<PaginatedResponse>() {
        @Override
        public PaginatedResponse createFromParcel(Parcel in) {
            return new PaginatedResponse(in);
        }

        @Override
        public PaginatedResponse[] newArray(int size) {
            return new PaginatedResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        final Class<?> objectsType = artObjects.get(0).getClass();
        parcel.writeSerializable(objectsType);
        parcel.writeList(artObjects);
        parcel.writeLong(count);
    }
}
