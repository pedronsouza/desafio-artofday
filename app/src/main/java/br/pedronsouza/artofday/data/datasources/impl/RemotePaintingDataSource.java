package br.pedronsouza.artofday.data.datasources.impl;


import javax.inject.Inject;

import br.pedronsouza.artofday.ArtOfDayApplication;
import br.pedronsouza.artofday.BuildConfig;
import br.pedronsouza.artofday.data.datasources.PaintingDataSource;
import br.pedronsouza.artofday.data.net.ApiGateway;
import br.pedronsouza.artofday.data.net.helpers.PaginatedResponse;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import rx.Observable;

public class RemotePaintingDataSource implements PaintingDataSource {
    private ApiGateway gateway;

    @Inject public RemotePaintingDataSource(ApiGateway gateway) {
        this.gateway = gateway;
    }

    @Override
    public Observable<PaginatedResponse<PaintingModel>> getLatestsPaitings(int page) {
        return this.gateway.paitings().getLatestCollectionPaintings(page, ArtOfDayApplication.defaulPageSize, BuildConfig.DEFAULT_API_FORMAT, BuildConfig.API_KEY);
    }
}
