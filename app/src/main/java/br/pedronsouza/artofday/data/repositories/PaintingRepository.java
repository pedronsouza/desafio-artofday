package br.pedronsouza.artofday.data.repositories;

import javax.inject.Inject;

import br.pedronsouza.artofday.ArtOfDayApplication;
import br.pedronsouza.artofday.data.datasources.PaintingDataSource;
import br.pedronsouza.artofday.domain.viewmodels.LatestPaintingsViewModel;
import rx.Observable;

public class PaintingRepository {
    private PaintingDataSource dataSource;

    @Inject public PaintingRepository(PaintingDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Observable<LatestPaintingsViewModel> getLatestPaitings(int page) {
        return this.dataSource.getLatestsPaitings(page).flatMap((map) -> {
            boolean hasMorePages = (map.getCount() / ArtOfDayApplication.defaulPageSize) > page;
            return Observable.just(new LatestPaintingsViewModel(
                                    hasMorePages,
                                    page,
                                    map.getArtObjects()));
        });
    }
}
