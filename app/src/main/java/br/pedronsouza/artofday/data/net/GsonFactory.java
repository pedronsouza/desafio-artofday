package br.pedronsouza.artofday.data.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by pedronsouza on 02/12/16.
 */

public class GsonFactory {
    public static Gson create() {
        return new GsonBuilder().create();
    }
}
