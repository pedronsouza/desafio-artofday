package br.pedronsouza.artofday.data.datasources;

import br.pedronsouza.artofday.data.net.helpers.PaginatedResponse;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import rx.Observable;

public interface PaintingDataSource {
    Observable<PaginatedResponse<PaintingModel>> getLatestsPaitings(int page);
}
