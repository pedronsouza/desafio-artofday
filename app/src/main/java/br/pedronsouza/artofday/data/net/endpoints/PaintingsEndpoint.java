package br.pedronsouza.artofday.data.net.endpoints;

import br.pedronsouza.artofday.data.net.helpers.PaginatedResponse;
import br.pedronsouza.artofday.domain.model.PaintingModel;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface PaintingsEndpoint {
    @GET("collection")
    Observable<PaginatedResponse<PaintingModel>> getLatestCollectionPaintings(
            @Query("p")         int page,
            @Query("ps")        int size,
            @Query("format")    String format,
            @Query("key")       String key);
}
